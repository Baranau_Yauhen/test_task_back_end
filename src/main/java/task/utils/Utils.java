package task.utils;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import task.model.User;


import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static task.Constants.*;

public class Utils {

    public static Long getResultsCount(String firstName, String lastName) {
        Long result = 0L;
        try {
            final String url = "https://www.google.com/search?q=" + firstName + "%20" + lastName;
            try (final WebClient webClient = new WebClient()) {
                final HtmlPage page = webClient.getPage(url);
                final HtmlDivision div = page.getHtmlElementById(ID_ELEMENT);
                String s1 = div.getTextContent();
                s1 = s1.replaceAll(SPACE,EMPTY);
                Pattern pat=Pattern.compile(REGEX_COUNT_RESULTS);
                Matcher matcher=pat.matcher(s1);
                StringBuilder builder = new StringBuilder(EMPTY);
                while (matcher.find()) {
                    builder.append(matcher.group().trim());
                }
                result = Long.parseLong(builder.toString());
            }
        } catch (IOException ignored) {
        }
        return result;
    }

    public static User parceStringToUser(String s) {
        User user = new User();
        String[] userLine = s.split(DELIMITER);
        if (userLine[0].equals(ID)) {
            return user;
        }
        long id = Long.parseLong(userLine[0]);
        String firstName = userLine[1];
        String lastName = userLine[2];
        String eMail = userLine[3];
        String gender = userLine[4];
        String ipAddress = userLine[5];
        user.setId(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.seteMail(eMail);
        user.setGender(gender);
        user.setIpAddress(ipAddress);
        return user;
    }
}
