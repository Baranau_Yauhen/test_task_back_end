package task;

import org.springframework.context.support.GenericXmlApplicationContext;
import task.interfaces.UserService;
import task.model.User;
import task.utils.Utils;

import java.io.*;
import java.util.concurrent.*;


public class Runner {

    public static void main(String[] args) throws IOException {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:applicationContext.xml");
        ctx.refresh();
        UserService service = ctx.getBean("userService", UserService.class);
        FileReader fileReader = ctx.getBean("fileReader", FileReader.class);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        ExecutorService executor = ctx.getBean("taskExecutor", ThreadPoolExecutor.class);
        String s;
        while ((s = bufferedReader.readLine()) != null) {
            String finalS = s;
            executor.submit(() -> {
                User user = Utils.parceStringToUser(finalS);
                if (user.getFirstName() != null) {
                    user.setResultsCount(Utils.getResultsCount(user.getFirstName(), user.getLastName()));
                    service.addUser(user);
                }
            });
        }
        bufferedReader.close();
        executor.shutdown();
    }

}
