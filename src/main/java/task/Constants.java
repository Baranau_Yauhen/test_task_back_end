package task;

public class Constants {

    public final static String DELIMITER = ",";
    public final static String ID = "id";
    public final static String ID_ELEMENT = "resultStats";
    public final static String EMPTY = "";
    public final static String SPACE = "\u00a0";
    public final static String REGEX_COUNT_RESULTS = "\\s\\d+";

}
